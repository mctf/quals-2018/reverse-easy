#!/usr/bin/python
import mctf
import sys

if __name__ == "__main__":
    if len(sys.argv) < 2:
        print "Usage: task.py <flag>"
        sys.exit(0)
    Mctf = sys.argv[1]
    mctf.mctf(Mctf)
