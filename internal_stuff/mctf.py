#mctf{4995a912cd9ed310ba403f632a525c76}

import sys
import platform
from ctypes import *

def a():
    return platform.machine().endswith('64')

def c(var1, var2):
    var1 += 1
    var1 -= 1
    return ~(~var1 + var2)

def mctf(t):
    e = 1
    e += 1
    if "YmRw".decode("base64")[::-1] in sys.modules.keys():
        print "Nope!"
        sys.exit()

    if "win" in sys.platform:
        d = windll.kernel32.IsDebuggerPresent()
        if d:
            print "Nope!"
            sys.exit()
    else:        
        l = CDLL(None)
        s = l.syscall
     
        if a():
            if s(101, 0, 0, 1, 0) < 0:
                print "Nope!"
                sys.exit()
        else:
             if s(26, 0, 0, 1, 0) < 0:
                print "Nope!"
                sys.exit()         
    k = "h"
    r = [-5, 5, -12, 2, -19, 52, 47, 47, 51, 7, 47, 55, 54, 5, 4, 47, 3, 4, 53, 55, 56, 6, 7, 52, 56, 53, 2, 50, 53, 54, 7, 51, 54, 51, 5, 49, 50, -21]

    if len(t) != len(r):
        print "Nope!"
        sys.exit()

    for i in xrange(len(t)):
        if r[i] != (c(ord(k), ord(t[i]))):
            print "Nope!"
            sys.exit()

    print "You win!"
    


